package org.myownstock.user.users;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter @Setter
@Entity
@Table(name="user")
public class Users
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(length=75, nullable=false)
    private String lastname;

    @Column(length=75, nullable=false)
    private String firstname;

    @Column(nullable=false)
    private LocalDate birthdate;
}