package org.myownstock.user.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UsersController {
    @Autowired
    UsersService service;
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Users add(@RequestBody Users users) {
        return service.add(users);
    }

    @GetMapping
    List<Users> findAll() {
        return service.findAll();
    }
}
