package org.myownstock.user.users.impl;

import org.myownstock.user.users.IUsers;
import org.myownstock.user.users.Users;
import org.myownstock.user.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    private IUsers repository;
    
    @Override
    public Users add(Users users) {
        return repository.save(users);
    }

    @Override
    public List<Users> findAll() {
        return repository.findAll();
    }
}