package org.myownstock.user.users;

import org.myownstock.user.communities.helpers.services.Service;

import java.util.List;

public interface UsersService extends Service<Users> {
    Users add(Users users);
    List<Users> findAll();
}