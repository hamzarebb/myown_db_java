package org.myownstock.user.users;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IUsers extends JpaRepository<Users, Long> {
}