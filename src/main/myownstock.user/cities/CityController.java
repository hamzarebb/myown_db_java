package myownstock.user.cities;

import springframework.beans.factory.annotation.Autowired;
import springframework.http.ResponseEntity;
import springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cities")
public class CityController
{
    @Autowired
    CityService cityService;
    @PostMapping
    public City add(@RequestBody City city)
    {
        return cityService.add(city);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody City city)
    {
        return this.cityService.update(city);
    }

    @GetMapping
    public List<City> findAll(){
        return cityService.findAll();
    }
}