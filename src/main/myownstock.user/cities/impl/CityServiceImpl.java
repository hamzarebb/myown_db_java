package myownstock.user.cities.impl;

import myownstock.user.cities.City;
import myownstock.user.cities.CityService;
import myownstock.user.cities.ICity;
import springframework.beans.factory.annotation.Autowired;
import springframework.http.ResponseEntity;
import springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private ICity repository;
    @Override
    public City add(City city) {
        city = repository.save(city);
        return city;
    }

    @Override
    public ResponseEntity<?> update(City city) {
        this.repository.save(city);
        return ResponseEntity.ok().build();
    }

    @Override
    public List<City> findAll()
    {
        return this.repository.findAll();
    }
}
