package org.myownstock.user.cities;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CityService
{
    /**
     * Design a method to add a new city
     * @param City city
     * @return City
     **/
    public City add(City city);

    /**
     * Designated method to update a city
     * @param City city
     * @return ResponseEntity
     **/
    public ResponseEntity<?> update(City city);
    /**
     * Find all the cities.
     *
     * @return         List of all cities
     */
    public List<City> findAll();
}