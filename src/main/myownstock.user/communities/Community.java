package org.myownstock.user.communities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.myownstock.user.cities.City;

import java.io.Serializable;

@Getter @Setter
@Entity
public class Community implements Serializable
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="label", length=150, nullable=false)
    private String label;

    @Column(name="street_number")
    private Integer streetNumber;

    @Column(name="address1", length=255)
    private String address;

    @Column(name="address_detail", length=255)
    private String addressDetail;

    @ManyToOne(fetch=FetchType.LAZY, optional=false)
    @JoinColumn(name="city_id")
    @JsonIgnoreProperties(value={"hibernateLazyInitializer", "handler"}, allowSetters=true)
    private City city;
}