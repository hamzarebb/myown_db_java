package org.myownstock.user.communities.helpers.services;

public interface Service <T>{
    T add(T t);
}