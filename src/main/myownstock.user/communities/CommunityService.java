package org.myownstock.user.communities;

import java.util.List;

public interface CommunityService {

    /**
     * Design a new method to add a new community
     * @param Community community
     * @return Community
     **/
    public Community add(Community community);

    public List<Community> findAll();
}