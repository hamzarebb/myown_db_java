package org.myownstock.user.communities;

import org.myownstock.user.communities.helpers.services.Service;

public interface UserToCommunityService extends Service<UserToCommunity>
{
}
